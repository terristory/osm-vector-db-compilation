#!/bin/bash

set -e
set -u

start=`date +%s`

if [ ! -d "openmaptiles-tools" ] ; then
    git clone https://github.com/openmaptiles/openmaptiles-tools.git
fi
if [ ! -d "openmaptiles" ] ; then
    git clone https://github.com/openmaptiles/openmaptiles.git
fi

cd openmaptiles && git fetch
git checkout v3.10

# download OSM data
while getopts f:s: flag
do
    case "${flag}" in
        f) force=${OPTARG};;
        s) finalsql=${OPTARG};;
    esac
done

if [ -v force ]; then
    echo "****************************************"
    echo "****************************************"
    echo "Forces download"
    echo "****************************************"
    echo "****************************************"
    rm -rf data/*
else
    echo "****************************************"
    echo "****************************************"
    echo "Keep downloaded data"
    echo "****************************************"
    echo "****************************************"
fi

if [ ! -f "data/france-latest.osm.pbf" ] ; then
    wget -P data/ http://download.geofabrik.de/europe/france-latest.osm.pbf
else
    echo "****************************************"
    echo "****************************************"
    echo "Doesn't need to download France already present"
    echo "****************************************"
    echo "****************************************"
fi


cd data/

# merge multiple pbf to a single one
if [ ! -f "data/terristory.pbf" ]; then
    echo "****************************************"
    echo "****************************************"
    echo "osmium merges all OSM pbfs files"
    echo "****************************************"
    echo "****************************************"
    osmium merge france-latest.osm.pbf -o terristory.pbf
    echo "****************************************"
    echo "****************************************"
    echo "Removes data"
    echo "****************************************"
    echo "****************************************"
    # rm france-latest.osm.pbf 
else
    echo "****************************************"
    echo "****************************************"
    echo "Merges already done"
    echo "****************************************"
    echo "****************************************"
fi
cd ../

# Storing the gateway IP of the Docker bridge interface to be able to connect from
# the container to our local PostgreSQL
export PG_GATEWAY=$(docker network inspect bridge | jq -r .[0].IPAM.Config[0].Gateway)
echo "****************************************"
echo "****************************************"
echo "PG_GATEWAY: ${PG_GATEWAY}"
echo "****************************************"
echo "****************************************"

# tm2source and collect all SQL scripts
if [ ! -f "build/openmaptiles.tm2source/data.yml" ] ; then
    mkdir -p build/openmaptiles.tm2source
    echo "****************************************"
    echo "****************************************"
    echo "Building tm2source"
    echo "****************************************"
    echo "****************************************"
    docker run -v $(pwd):/tileset -v $(pwd)/build:/sql --rm openmaptiles/openmaptiles-tools generate-tm2source openmaptiles.yaml --port=5432 --database="build_osm" --user="osm" --password="Eishae6s" > build/openmaptiles.tm2source/data.yml
fi

if [ ! -f "build/mapping.yaml" ] ; then
    echo "****************************************"
    echo "****************************************"
    echo "Building imposm3"
    echo "****************************************"
    echo "****************************************"
    docker run -v $(pwd):/tileset -v $(pwd)/build:/sql --rm openmaptiles/openmaptiles-tools generate-imposm3 openmaptiles.yaml > build/mapping.yaml
fi

if [ ! -f "build/tileset.sql" ] ; then
    echo "****************************************"
    echo "****************************************"
    echo "Building sql"
    echo "****************************************"
    echo "****************************************"
    docker run -v $(pwd):/tileset -v $(pwd)/build:/sql --rm openmaptiles/openmaptiles-tools generate-sql openmaptiles.yaml > build/tileset.sql
fi


# Load OSM dataset. By default, the pbf file which is imported to the db is the one which is in the 'data' folder.
echo "****************************************"
echo "****************************************"
echo "Mapping"
echo "****************************************"
echo "****************************************"
docker run --rm --network="host" -v $(pwd)/data:/import -v $(pwd)/build:/mapping -e POSTGRES_DB="build_osm" -e POSTGRES_PORT="5432" -e POSTGRES_HOST="127.0.0.1" -e POSTGRES_PASSWORD="Eishae6s" -e POSTGRES_USER="osm" -e DIFF_MODE="FALSE" openmaptiles/import-osm:0.5
echo "****************************************"
echo "****************************************"
echo "Generating osm border"
echo "****************************************"
echo "****************************************"
docker run --rm --network="host" -v $(pwd)/data:/import openmaptiles/generate-osmborder
echo "****************************************"
echo "****************************************"
echo "Generating osm border (suite)"
echo "****************************************"
echo "****************************************"
docker run --rm --network="host" -v $(pwd)/data:/import -e POSTGRES_DB="build_osm" -e POSTGRES_PORT="5432" -e POSTGRES_HOST="127.0.0.1" -e POSTGRES_PASSWORD="Eishae6s" -e POSTGRES_USER="osm" openmaptiles/import-osmborder:0.4

# Download additional non-OSM data
echo "****************************************"
echo "****************************************"
echo "Generating osm water"
echo "****************************************"
echo "****************************************"
docker run --rm --network="host" -e POSTGRES_DB="build_osm" -e POSTGRES_PORT="5432" -e POSTGRES_HOST="127.0.0.1" -e POSTGRES_PASSWORD="Eishae6s" -e POSTGRES_USER="osm" openmaptiles/import-water:1.1
echo "****************************************"
echo "****************************************"
echo "Generating osm natural earth"
echo "****************************************"
echo "****************************************"
docker run --rm --network="host" -e POSTGRES_DB="build_osm" -e POSTGRES_PORT="5432" -e POSTGRES_HOST="127.0.0.1" -e POSTGRES_PASSWORD="Eishae6s" -e POSTGRES_USER="osm" openmaptiles/import-natural-earth:1.4
echo "****************************************"
echo "****************************************"
echo "Generating osm lakelines"
echo "****************************************"
echo "****************************************"
docker run --rm --network="host" -e POSTGRES_DB="build_osm" -e POSTGRES_PORT="5432" -e POSTGRES_HOST="127.0.0.1" -e POSTGRES_PASSWORD="Eishae6s" -e POSTGRES_USER="osm" openmaptiles/import-lakelines:1.0

# Grab wikipedia data (30Gb, quite long).
# skip the downloading part.
# docker run --rm --network="host" -v $(pwd)/wikidata:/import --entrypoint /usr/src/app/download-gz.sh openmaptiles/import-wikidata:0.1
# juste create the tables structure in the db
echo "****************************************"
echo "****************************************"
echo "Final data import"
echo "****************************************"
echo "****************************************"
docker run --rm --network="host" -e POSTGRES_DB="build_osm" -e POSTGRES_PORT="5432" -e POSTGRES_HOST="127.0.0.1" -e POSTGRES_PASSWORD="Eishae6s" -e POSTGRES_USER="osm" openmaptiles/import-data

 # Load sql wrappers used for rendering
echo "****************************************"
echo "****************************************"
echo "Final sql import"
echo "****************************************"
echo "****************************************"
docker run --rm --network="host" -v $(pwd)/build:/sql -e POSTGRES_DB="build_osm" -e POSTGRES_PORT="5432" -e POSTGRES_HOST="127.0.0.1" -e POSTGRES_PASSWORD="Eishae6s" -e POSTGRES_USER="osm" openmaptiles/import-sql


end=`date +%s`

echo 'total duration: ', $((end-start))
