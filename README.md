Tools
===

* OpenMapTiles : https://github.com/openmaptiles/openmaptiles-tools

Requirements
=====

OSML10n
--- 

Pas réussi à faire fonctionner cette version :

* `git clone https://github.com/giggls/osml10n.git` et `https://github.com/giggls/osml10n/blob/master/INSTALL.mds`
 
Used (according to `https://github.com/giggls/mapnik-german-l10n/blob/master/INSTALL.md`):

* `sudo apt install postgresql-server-dev-12 postgresql-server-dev-all`
* `sudo apt-get install devscripts equivs`
* `sudo mk-build-deps -i debian/control`
* download `https://www.nominatim.org/data/country_grid.sql.gz` and extract sql file inside root folder of mapnik git repo
* `git clone https://github.com/giggls/mapnik-german-l10n.git`
* then `make deb`
* then `sudo dpkg -i ../postgresql-12-osml10n_2.5.10_amd64.deb`

Osmium
--- 

* `sudo apt install osmium-tool`
* `sudo apt-get install jq`

PostgreSQL user/database
===

```sql
CREATE DATABASE build_osm;
CREATE USER osm WITH PASSWORD 'Eishae6s';
GRANT ALL PRIVILEGES ON DATABASE build_osm TO osm;
\c build_osm
CREATE EXTENSION postgis CASCADE;
CREATE EXTENSION osml10n CASCADE;
```

Run
===

`./build_osm.sh > log`

Troubleshooting
===

* Connection to local database : cf. https://stackoverflow.com/questions/24319662/from-inside-of-a-docker-container-how-do-i-connect-to-the-localhost-of-the-mach